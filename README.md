# Muze Changelogs

## v3.0.1

### Changes
- Make npm folder as an installable package
- Fix column resize event listeners in multiple chart #885
- Disabling interaction on column resize #852
- Continuous legend isn't honoring `useUTC` flag #10741
- Fix manipulated column widths being overridden by the widths of continuous axes #882
- Dual-axis not visible and marks don't show in one case #826
- Missing values are sorted first when ordering colors, last when ordering sizes #738
- Column widths reset on resize #10999
- Persist chart state throughout chart lifecycle on resize #842
- Column Totals not shown if there is no x-axis #883
- Changing DATEPART to DATETRUNC generates error #889
- Muze crash when a continuous temporal field is being used in text channel #10899
- Collapsing the right pane loses the legend filter state #546

## v3.0.0

### Features
- #842 persisting scroll state in canvas reflow:

```
canvas.config({
  scrollBar: {
    vertical: {
      initialScrollPercent: 50,
    },
    horizontal: {
      initialScrollPercent: 50,
    },
  },
})
```

### Bugfixes
- #890 totals resize
- Handle dispose when canvas not yet rendered
- #883 Show totals when no discrete value
- Other bugs with totals and resize

## v3.0.0-alpha-17

### Features
- Muze API to support use of Measure Names/Measure Values in Pivot Tables#834
- Pass data index to tooltip formatter#477
  API Description and migration doc : https://www.notion.so/charts/List-of-the-formatter-callbacks-supported-in-Muze-config-0472b93646294f33877c7c4c66698941

### Bugfixes
- Totals are incorrect when there is split totals on both columns and rows.#876
- Axis space is not zero for univariate viz.#828
- First facet can't be resized#874
- Trailing garbage in Pie Chart#727
- Tooltip for missing continuous color displays "NaN" on legend.#731

## v3.0.0-alpha-16

### Features
- Multiple totals are not handled properly #823
```js
[rows/columns]: {
  totals: {
     grandTotals: {
        enabled: true,
        text: 'Grand Totals', 
        splitTotal: {
             field: 'MeasureName', // Field Name to be used for spliting the totals .
             text: 'Total' // Label to be shown for the MeasureName totals
        }
    },
    data: {
        dataModel: totalsDataModel
    }
}
```

- Modification in the structure of the object returned in column resize call back
```js
.config({
                columns:{
                    layout: [],
                },
                interaction: {
                    resize: {
                        enabled: true,
                        sideEffects: {
                            'column-dimension-changer': {
                                widths: (state) => {
                                    const widths = state.serialize();
                                    console.log('widths', widths);
                                    /** Structure of widths
                                        [
                                        target: [{
                                                field: 'MeasureName',
                                                value: 'Profit',
                                                sourceType: 'total'  // New Property Added
                                            },
                                            {
                                                field: 'Segment'
                                            }
                                        ],
                                        width: 100
                                        ]
                                    */
                                }
                            }
                        }
                    }
                }
            },
)
```
The property is needed for detecting if the cell type is a total cell or a normal text cell. In case of axis values, sourceType is not present.

### Bugfixes
- Temporal columns can't be resized #872
- Continuous Legend does not respect date format #814
- Turning off column facet values hides row headers in faceted visualizations with more than one column #686

## v3.0.0-alpha-15-hotfixes

### Bugfixes
- Column width manipulation affordance display dependent on mouse pointer direction#849
- Non-nested column headers generate Javascript error#848
- Resizing bar chart column past minimum width generates JavaScript error#851
- Layout for unrelated field affects X axis#850

## v3.0.0-alpha-15

### Features
- Muze API to support direct column manipulation width #833
```js
.config({
            columns: {
                columns:{
                    layout: [ // Set dimensions of specified columns
                        {
                            "target": [
                                {
                                    "field": "Ship Mode",
                                    "value": "Second Class"
                                }
                            ],
                            "width": 500,
                            "height": 22
                        }
                    ],
                },
                interaction: {
                    resize: {
                        // Enable the resize interaction
                        enabled: true,
                        sideEffects: {
                            'column-dimension-changer': {
                                /**
                                    * Get the final column widths which is set on interaction.
                                    */
                                widths: (state) => {
                                    const widths = state.serialize();
                                    console.log('widths', widths);
                                }
                            }
                        }
                    }
                }
            },
})
```

- Nested Sort API for facets. #692
```js
{
    rows: {
        facets: {
        fields: {
            Segment: {
            ordering: {
                type: "nested",
                nested: { field: { name: <field_name>, aggregation: <aggregation type> } },
            },
            },
        },
        },
    },

```

- New lifecycle api and hooks for Muze #741
    https://www.notion.so/charts/New-Lifecycle-API-Specs-d531b04ba4fd4aa3a1480fff64f3cec3


# Bug Fixes
- Rotated bar labels may be truncated #822
- Scrollable area for table is only over the table#550
- Labels provided in text encoding gets clipped for charts when length of label is more than the range of the chart.#827
- Horizontal bar chart labels are clipped #730

# CI/CD
- Enable Pipeline between Mode and Muze for automated builds and code contribution #466

## v3.0.0-alpha-14

### Bugfixes
- Issue #816
- Issue #740

### Features
- API added, #815
Modify default column width 
```jss
.layers([
    {
        mark : "text",
        encoding: {
            text: {
                field : 'review',
                charsAcross: { // new API
                    min : 8,   // default min
                    max : 18   // default max
                }
            }
        }
    }
])
```
- API added, #771
```js
canvas.config({
    legend: {
        interaction: {
            // Inverse select behaviour gets triggered on legend
            'inverse-select': {
                // target property defines the behavioural actions which will get triggered on target components
                target: {
                    layer: {
                        // Dispatch filter behaviour on layers when legend is clicked
                        filter: {
                            sideEffects: {
                                // This filters the bars and updates the axis domains
                                filter: {
                                    // If lockDomain is true, then the axis domain does not change.
                                    lockDomain: true
                                },
                                // Facet axis highlighter is a side effect which fades out the unselected axis labels and facets
                                'facet-axis-highlighter': {
                                    enabled: true
                                },
                            }
                        }
                    }
                }
            }
        }
    }
})
```
- Disable rotation config for pivot tables
- Fixed label truncation for bar charts

## v3.0.0-alpha-13-hotfixes

### Bugfixes
- Minimum width of columns in pivot table increased to 7 Characters.
- Handle Muze when mount point becomes null.
- Always set strings as innerText rather than innerHtml in order to prevent xss attacks.

## v3.0.0-alpha-13

### Features
- Refined pivot table styling.

## v3.0.0-alpha-12

### Better Pivot table styling 
- Row header is not vertically aligned #547
- Pivot Table column width choice sometimes truncates data, removes padding #694
- Pivot Table data alignment based on data type #696
- Row headers are now left aligned by default.
- Added buffer space in text lables to consume any addition space required due to custon style 

### Bugs
- Bar/Area charts faile with non-continuous "Text" field and Measure in "Row" #718
- Null header label doesn't bold on hover the way other labels do #724
- Tick Values in axis fixed.

## v3.0.0-alpha-11

### Features
- Color legends appear to not be sorted #551

### Bugfixes
- Custom sort with discrete numeric data is not working #704
- Field level classname for facets

## v3.0.0-alpha-10

### Features
- Added UTC supports in Muze.

API
```js
canvas.config({
    useUTC: true,
})
```

- Sort axes in a non-faceted chart. #645

API:
```js
axes : {
    x : {
        fields: {
            // Only for 'discrete' field.
            fieldName: {
                ordering: {
                    type: 'natural' | 'alphabetical' | 'field' | 'custom', // default is 'natural'.
                    direction: 'asc' | 'desc', // default is 'asc'. Valid only for 'natural', 'alphabetical' and 'field' types.
                    field: { name: "<fieldName>", aggregation: "<avg|sum|etc>"  }, // valid only for 'field' type
                    values : ["<domain_order_values>"] // valid only for 'custom' type. Give any invalid values as `null` value.
                }
            }
        }
    }
}
```

- Sort/Arrange facets in a faceted charts #646
API
```js
rows/columns : {
    facets : {
        fields: {
            // Only for 'discrete' field.
            fieldName: {
                ordering: {
                    type: 'natural' | 'alphabetical' | 'field' | 'custom',  // default is 'natural'.
                    direction: 'asc' | 'desc', // default is 'asc'. Valid only for 'natural', 'alphabetical' and 'field' types.
                    field: { name: "<fieldName>", aggregation: "<avg|sum|etc>"  }, // valid only for 'field' type
                    values : ["< domain_order_values>"] // valid only for 'custom' type. Give any invalid values as `null` value.
                }
            }
        }
    }
}
```

You can find the detailed definition of various sort order in [here](https://charts.notion.site/Sorting-Specs-a595501ea5994a0ba930dda169d188fb).

### Bugs
- Select interaction bug for line layer in dual axis scenario
- Turning off column facet values hides row headers #647
- Remove user data from DOM #402

## v3.0.0-alpha-9

### Bugfixes
1. Header values in nested pivot table are getting truncated #575
2. Investigate performance issues with deeply nested pivot tables #5203
3. Error rendering Pivot Table with Totals and no Column assignment #562
4. Other minor bugs

## v3.0.0-alpha-8

### Features
- Bars: Auto-rotate labels #472
API
```js
    {
        mark: "bar",
        encoding: {
          text: { 
              field: "Worldwide_Gross", 
              rotation: "auto" 
          },
        },
    }
```

- Lines/Circles/Bubbles: avoid over plotting #497

- Hide scroll buttons in Muze scroll bar #612
API
```js
    scrollBar: {
        buttons: {
            show: false
        }
    }
```

- Maintain interaction state while scrolling #567

- Support minimum width for Pivot Table columns #215
API 
```js
    {
        "rows": {
            "headers": {
                "charsAcross": {
                    "min": <number>, // Default 4
                    "max": <number> // Default 8
                }
            }
        }
    }
```

### Bugs
- Domain Extension for line is more than required #552
- Muze: Crash using facet formatter when data contains nulls #545
- Muze 'headers' config doesn't work when 'facets' is also used #582
- Row headers config isn't honored for discrete row field on axis #583

### Known Issues
- User given `tickValues` in axis config does not work.

## v3.0.0-alpha-7

### Features
- Show/hide axis API per field #422
API:
```js
const config = {
    rows: {
        facets: {
            show: true,
            labels: {
                align: LEFT,
            },
            className: "abc-rows",
            format: () => {},
            fields : {
                "Maker" : {
                    show: true,
                    labels: {
                        align: LEFT,
                    },
                    className: "xyz-column-maker",
                    format: () => {},
                }
            }
        },
        headers: {
            show: true,
            position: TOP,
            fields: {},
        }
    },
    columns: {
        facets: {
            show: true,
            verticalAlign: MIDDLE,
            labels: {
                align: LEFT,
            },
            className: "abc-columns",
            fields: {
                "Origin" : {
                    show: true,
                    labels: {
                        align: LEFT,
                    },
                    className: "xyz-column-origin",
                    format: () => {},
                }
            }
        },
        headers: {
            show: true,
            position: TOP,
            align: CENTER,
            className: `${custom}-column-header-container`,
            formatter: (fields) => fields.join(" / "),
            fields: {
                    "Quantity": {
                    show: true,
                },
            },
        }
    },
}
```
- Handle Hover interaction. #529
- Dispose intermediately datamodel created for grand-totals. #560

### BugsFixes
- (8) Line chart turns into a dot plot when a dimension is used as Label #475
- (8) The Last Column (of a Continuous Field) is sometimes missing labels #379

### Deprecated API
1.
```js
.config({
        axesHeaders: {
            column: {
               show: true/false, // Show / hide the column axis header.
               align: 'center', // Changes alignment of the header text.
               padding: 0,
               className: 'MODE', // Sets className for the column axis header.
               formatter: (value) => `${value} --------`
            }
         }
     });
```
2.
```js
    config({
       facets: {
           rows: {
               labels: {
                    align : "right", // Sets the text alignment of the row facet labels
                },
                className: 'muze-row-facet-label' // Adds a custom className for the row facet labels.
            },
            columns: {
                labels: {
                    align : "right", // Sets the text alignment of the column facet labels
                },
                className: 'muze-row-facet-label' // Adds a custom className for the column facet labels.
            }
    })
```
3. 
```js
    config({
        facets: {
            format: {
                // FieldName : Formatter
                Year: (x) => `Hello-${x}`,
            },
        }
    })
```
### Migration
- `axesHeaders` is changed to  `headers` under row/column->headers
- `facets` under config is moved to row/columns->facets
- `facets.format` is moved to row/columns->facets.format

## v3.0.0-alpha-6

### Features
1. Reduce label clipping for line layer
2. Bars: change default for labels to be on top of bars #471
API:
```js
{
    encoding: {
        text: {
            field: 'SALES',
            labelPlacement: {
                anchor: ['top-outside', 'center'],
            },
        },
    }
}
```
3. Aggregation support for grand totals
```js
{
    totals: {
        aggregations: {
            "<fieldName>": "<aggregationName>" // All datamodel supported aggregations
        }
    }
}
```

### Bugfixes
1. Line chart labels disappear when changing format, resizing window #479
2. Zebra striping should be determined across the entire table #517
3. Header is showing our internal column name instead of display name #509
4. Dual Axis is not properly scaled when Negative. #489

## v3.0.0-alpha-5

### Features
1. New cross interaction API
2. UniformAxisDomain per field #444
3. Virtual Scrolling per Unit
4. Grand Totals across layers

### Bugfixes
1. Chrome does not support more than 1000 rows in css grid. #253
2. Axis values get cut off #446

## v3.0.0-alpha-4

### Features
1. Crash with temporal field #370
2. Default display order of values is wrong when there are missing values in first panes #347
3. Tooltip shows internal id for row/column dimensions #426

### Bugfixes
1. Some other bugfixes.

## v3.0.0-grand-total-1

### Features
1. Support for Grand Totals in Pivot table.
```js
.config({
    rows: {
    totals: {
        grandTotals: {
        enabled: true,
        text: "Grand Average"
        },
        data: {
            dataModel: rowTotaldmInstance,
            totalString: "All"
        },
    },
    },
    columns: {
    totals: {
        grandTotals: {
        enabled: true,
        text: "Grand Average",
        },
        data: {
            dataModel: columnTotalDmInstnace,
            totalString: "All"
        },
    },
    },
})
```

### Bugfixes
1. Stack order does not match with legend label order when autoSort is disabled #338
The new API to sort the legend label values to match with the stack order:
```js
legend: {
    color: {
        valueOrder:  'desc',
        fields: {
            Cylinders: {
                valueOrder:  'asc'
            }
        }
    }
}
```

## v3.0.0-alpha-3

### Features
1. Provide API to specify how to handle legend interactions #303

### Bugfixes
1. Domain does not extends after legend interaction for bubble and pie charts #321
3. Stack order does not match with legend label order when autoSort is disabled #338
4. Opacity Options #346
5. (14) Prevent browsers from caching stale versions of Muze files #360
6. Discrete headers should appear at the top of columns #340

## v3.0.0-alpha-2

### Bugfixes
1. Default text does not display in Pivot Table. #324

## v3.0.0-alpha-1

### Features
1. Added virtual scrolling major feature

### Known Issues
1. Hover interaction is laggy for very large data set
2. Interaction state is not maintained

## v2.1.0

### Features
1. Text Mark with a Continuous Axis has no Hover State #207.

### Bugs
1. Another piece of pie #221.
2. Sanitize (XSS) content provided by the user before adding to DOM #270.
3. Right width handle does not work at all for Muze visualizations in the Report Builder #260.
4. Headers shouldn't dictate width of columns #215.


## v2.0.0-beta-15

### New features
1. Text Truncating Rules for pivot table. #167
2. Rows and Columns take only space required for its content and not all the available space.

### Bugs
1. Layout of Row Facets breaks for smaller charts , with multi-level facets. #230
2. Legend Marker not visible, legend field is not present in chart. #201

## v2.0.0-beta-14

### Bug Fixes:
* Setting domain as `[min, null]` inverts the charts #203.
* Domain option in axis config should respect null values #198.
* Tooltip of bars goes out of viewport when domain is reduced below the bar height #202.
* Discrete temporal field as outer discrete value in rows or columns throws an error: t.indexOf() is not a function #200.
* Line mark with only one datapoint should still render something #110.
* Null values in Pie mark should not be rendered #114.
* Missing value is not plotted #222.
* Visualization should still render when there is only one discrete field in Rows or Columns #83.

## v2.0.0-beta-13

### New features

1. Added field level configuration support in axis.
```js
.config({
        axes: {
           x/y: { 
              fields: {
                 "FIELD_NAME": {
                    tickFormat: () => {},
                    domain: [min, max] // Only for continuous axis.
                    showAxisName: true/false, // hides the axis name.
                    show: true/false, // Hides the entire axis.
                    labels: {
                        rotation: degree // Rotation angle in degrees
                    },
                    interpolator: linear/log/pow // Specifies the scale type. Only works for continuous axis.                       
                 }
              }
           }
        }
})
```

2. Added field level configuration for legend.
```js
.config({
        legend: {
           color: { 
              fields: {
                 "FIELD_NAME": {
                    title: '', // Legend title
                    position: 'top' // Legend position.
                    show: false // Hides the legend.
                    text: {
                       // Returns the formatted value to be shown in legend item.
                       formatter: (value, domain, dataModelInstance) => {
                           return `${val} $`;
                       }
                    }
                 }
              }
           }
        }
     });
```

3. Added column header configuration support.
```js
.config({
        axesHeaders: {
            column: {
               show: true/false, // Show / hide the column axis header.
               align: 'center', // Changes alignment of the header text.
               padding: 0,
               className: 'MODE', // Sets className for the column axis header.
               formatter: (value) => `${value} --------`
            }
         }
     });
```

4. Added facet labels configuration support.
```js
.config({
       facets: {
           rows: {
		       labels: {
                    align : "right", // Sets the text alignment of the row facet labels
                },
                className: 'muze-row-facet-label' // Adds a custom className for the row facet labels.
            },
            columns: {
                labels: {
                    align : "right", // Sets the text alignment of the column facet labels
                },
                className: 'muze-row-facet-label' // Adds a custom className for the column facet labels.
            }
        }
})
```

5. Added align attribute in text layer configuration. Works only in pivot tables.
```js
.layers([
     {
        mark: 'text',
        align: 'right'
     }
]);
```

6. Added API to format text in each cell 
```js
.layers([
			{
				mark : 'text',
				encoding:{
                    text: {
                        field: 'FIELD_NAME',
                        formatter : (val)=>`FORMATED-${val}`
                    },
				}
			}
])
```

### Bugs
1. Fixed row headers not taking displayName from schema.
2. Fixed broken axis labels on 180 degree rotation.
3. Fixed select interaction in pivot tables.

## v2.0.0-beta-12

### Pivot Table v1
General Usage:
```js
canvas
    .rows(["DISCRETE FIELDS"])
    .columns(["DISCRETE FIELDS"])
    .layers([
        {
            mark : 'text',
            encoding:{
                text : "< FIELD whose value to be shown >"
            }
        }
    ])
```

NOTE: To show axes headers at top of the table you need to pass the rows and columns as ( this is done to keep the API same with that of dual axis).

```js
canvas
    .rows([["DISCRETE FIELDS"],[]])
    .columns([["DISCRETE FIELDS"],[]])
    .layers([
        {
            mark : 'text',
            encoding:{
                text : "< FIELD whose value to be shown >"
            }
        }
    ])
```

To overwrite the css styling of zebra stripes , put you own css under these class names:
```js
.muze-text-row-odd {
    background-color: "";
}
.muze-text-row-even {
    background-color: "";
}
```

To show a default text when no field is given, you use the value api. If field is given then value of that field is taken:
```js
.layers([
    {
        mark: 'text',
        encoding: {
        text: {
                // field: "< FIELD whose value to be shown >",
                value: "Abc"
            }
        }
    }
])
```

## v2.0.0-beta-11

### Bugfix

1. Added `autoSort` feature. To disable all the default sorts in the chart,
the following config should be used:
```js
.config({
    sort: {
        autoSort: {
            disabled: true
        }
    }
})
```

## v2.0.0-beta-10

### BugFix
1. Dual Axis with `alignZeroLine` with null values.

### Features
1. API to disable all Sorts:
```js
.config({
    sort: {
        autoSort: {
            disabled: true
        }
    }	
})
```

2. Custom domain mapping for color range. User can specify a color for a specific domain value:
```js
.config({
    legend: {
        color: { 
            domainRangeMap: {
                // “domain-name” : “color”
                USA: “#E3E3E3”
            }
        }
    }
})
```

## v2.0.0-beta-9

### Features
1. Ability to set dual axis in custom order `#96`

```js
.columns([[ 'A' ,'B'],[null,'C'])
```

2. Added `dispose()` in canvas.
User can call this method when the chart unmounts or no longer required.
This method cleans and disposes off all objetcs and memory used.
API 

```js
canvas.dispose()
```

3. Added the ablity to stop all Aggregation 
This also solves the `_invalid ` issue in legend.
API

```js
.config({
    autoGroupBy:{
        disabled : true
    }
})
``` 

### Bugfixes
1. Fixed clipping of ARC and POINT layer when they are ploted in the extremes.
2. Fixed gradient legend height ,label overlapping and clipping of legend labels.
3. Prefer config styles to external CSS styles in axis tick labels and axis title.
This fix increase Muze ability to render more number of facets approximately 25-30K.

## v2.0.0-beta-8

### Features

1. Custom Formatting for facet headers:

```js
config({
    facets: {
        format: {
            // FieldName : Formatter
            Year: (x) => `Hello-${x}`,
        },
    }
})
```

2. Add `alignZeroLine` attribute in axis. This enables aligning of the zero line when dual axis is rendered.

```js
canvas.config({
   axes: {
      x: { alignZeroLine: true },
      y: { alignZeroLine: true }
   }
})
```

### Bug Fixes

1. Discrete numbers should sort numerically and not alphabetically `#84`
2. Negative values on grouped chart are not rendering properly `#101`
3. Legends render beyond the boundary of the container element `#68`
4. Title breaks on giving number
5. Format Date values on Facets Headers
6. Fix dual axis not rendering when there are unequal number of axes.
7. Fix wrong datamodel getting passed to layer when `canvas.transform` function is used and per layer encoding channel is used.

## v2.0.0-beta-7

### Bugs Fixes:
- Dual Axis Series Breaks When only one Series has a Color Channel.
- Long Tooltips expand beyond the chart container and are hidden or expose scroll-bars.
- x1 and x2 axes are swapped in the data.
- Call stack size exceeds limit with too many facets.
- Stacking does not work when continuous dimension is used in value field
- Axis - Labels overflow to certain extent
- Support for Continuous Dimension in DataModel
- Fix and refactor bar chart position calculation logic when continuous vs. continuous scale is plotted.
- Fix js error when a behavioural action is dispatched after canvas update

## v2.0.0-beta-6

### Support for continuous dimensions in rows and columns.

```js
 .rows([{ field: "Cylinders", as :'continuous' }])
```

### Support for discrete measures in color, size and shape encodings.

```js
.layers([
    {
        mark: "bar",
        encoding: {
            color: {
                field: "Acceleration",
                as: "discrete"
            }
        }
    }
])
```

### Support for continuous dimensions in color, size and shape encodings.

```js
.layers([
    {
        mark: "bar",
        encoding: {
            color: {
                field: "Cylinders",
                as: "continuous"
            }
        }
    }
])
```

### BugFixes.

## v2.0.0-beta-5

### API Changes

- Support for discrete measures in rows and columns:

```js
.rows([{ field : "Horsepower", as: "discrete" }])
```

- Repeat or linearly distribute the color range:

```js
config: {
    legend: {
        color: {
            fields: {
                Maker: {  // field name
                    repeatRange: true,
                },
            },
        },
    },
}
```

- Text Encoding for each layer:

```js
.layers([
    {
        mark: "<mark-type>",
        encoding: {
            text : "Origin",
        }
    }
])
```

### Bugs

- Animation happening for Pie, Area mark types and Axis ticks even with transition disabled.
For disabling axis animations, you need to pass:

```js
canvas.config({
    axes: {
        x: {
            transition: {
                disabled: true
            }
        },
        y: {
            transition: {
                disabled: true
            }
        }
    }
})
```

-  displayName not using the schema displayName.

## v2.0.0-beta-4

1. Add detail encoding support in layers. Detail should be passed as an array.

```js
canvas
.layers([
    {
        mark: "bar",
        encoding: {
            detail: ['Origin'],
        }
    }
])
```

2. Support interaction for multiple retinal encoding channels.

3. Add `getMarkInfFromLayers` api in visual unit.
This can be used to get the data of the interacted plot in physical action callback.

```js
muze.ActionModel.registerPhysicalActions({
    click: (firebolt) => (targetEl) => {
        targetEl.on('click', function (data) {
            const event = muze.utils.getEvent();
            const mousePos = muze.utils.getClientPoint(this, event);
            const payload = firebolt.getPayloadFromEvent('click', mousePos, { data, event });

            firebolt.triggerPhysicalAction('click', payload);
            const visualUnitInstance = firebolt.context;
            const markInf = visualUnitInstance.getMarkInfFromLayers(mousePos.x, mousePos.y, { event });

            console.log('Data', markInf.point.data);
        });
    },
})
```

4. Fixes dissociateSideEffect api.

5. Add `highlightExactPoint` attribute in layer config. When it is set to true, it only highlights and shows tooltip for the mark on which it is hovered on.
By default it is set to false for bar, line and area mark and true for arc, point and tick marks. 

```js
    canvas
    .rows(['Horsepower', 'Acceleration'])
    .columns(["Year"])
    .layers([
        {
          mark: "bar",
          encoding: {
            y: 'Horsepower',
            color: 'Maker',
            detail: ['Origin'],
          },
          transform: {
            type: 'group'
          },
          highlightExactPoint: true
        },
        {
          mark: "line",
          encoding: {
            y: 'Acceleration',
            color: 'Maker'
          },
          transform: {
            type: 'group'
          }
        },
    ])
```

6. Fixes highlight and select interactions for arc layer when continuous fields are present in rows or columns.


## v2.0.0-beta-3

1. Redesign Pie layer to work with other mark types e.g. bar, line etc.

```js
    canvas
    .rows(["Acceleration"])
    .columns(["Maker"])
    .layers(
        [
            {
                mark: 'arc',
                encoding: {
                    radius: "Horsepower",
                    angle: "Origin"
                }
            },
            {
                mark: 'bar',
            }
        ]
    )
    .color("Origin")
```

2. Size encoding for Bar layer.

```js
    canvas.layers(
        [
            {
                mark: 'bar'
            }
        ]
    )
    .size('Origin')
```

## v2.0.0-beta-2

1. Discrete Size Encoding for Line.

```js
    canvas.layers(
        [
            {
                mark: 'line'
            }
        ]
    )
    .size('Origin')
```

2. Layer wise encoding for color, shape and size, which will override the global encodings if specified.

```js
    canvas.layers(
        [
            {
                mark: 'line',
                encoding: {
                    color: "Origin",
                    size: "Acceleration"
                }
            },
            {
                mark: 'bar',
                encoding: {
                    color: "Cylinders",
                }
            },
        ]
    )
    .shape("Year");
```

3. Added supports for legend wise position.

```js
    canvas.config({
        legend: {
            color: {
                position: "top",
                fields: {
                    "Origin": {
                        position: "bottom"
                    }
                }
            },
            shape: {
                position: "right"
            },
            size: {
                position: "left"
            }
        }
    });
```

## v2.0.0-beta-1

1. Continuous size encoding in line:

```js
    canvas.layers(
        [
            {
                mark: 'line'
            }
        ]
    )
    .size('Horsepower')
```

2. Set uniform axis domains by default:

```js
    canvas
        .config(
            {
                uniformAxisDomains: false | true // Default is true
            }
        )
```

3. Configure axis rotation. Currently it works best for -90, 90, 0, 45 degree angles:

```js
    canvas
        .config(
            {
                axes: {
                    x: {
                        labels: {
                            rotation: 90
                        }
                    }
                }
            }
        )
```

4. Show/Hide headers:

```
canvas
    .config({
        axesHeaders : {
            column : {
                show : true | false // Default : true
            }
        }
    })
```

5. Code refactoring across muze packages.